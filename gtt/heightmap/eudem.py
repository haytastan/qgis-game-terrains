"""
    Create tilename for download from:
    This is the 2012rev1 version, which is NOT the latest copernicus version,
    however those files get unique download links
    
    * https://data.europa.eu/euodp/data/dataset/data_eu-dem
    * https://www.eea.europa.eu/data-and-maps/data/eu-dem#tab-original-data

    Resulting name something like:
    * http://published-files.eea.europa.eu/eudem/entr_r_4258_1_arcsec_gsgrda-eudem-dem-europe_2012_rev1/eudem_tiles_5deg/eudem_dem_5deg_n50e015.tif
"""

import math
from pathlib import Path

from .source import HeightmapSource, register_heightmap
from gtt.extent import transform_extent


@register_heightmap
class EudemHeightmap(HeightmapSource):
    BASEURL = "http://published-files.eea.europa.eu/eudem/entr_r_4258_1_arcsec_gsgrda-eudem-dem-europe_2012_rev1/eudem_tiles_5deg/"
    NAME = "eudem"

    def download(self, downloader):
        """Downloads the DEM files for current marked extent"""

        tiles = self.find_tiles()
        if tiles:
            for tilename in tiles:
                url = f"{self.BASEURL}{tilename}"
                file = self.get_folder(self.NAME) / tilename

                if not Path(file).exists():
                    credentials = (
                        "eea",
                        "http://published-files.eea.europa.eu",
                        "https://www.eea.europa.eu/themes/login_form",
                    )
                    downloader.get_file(url, file, credentials=credentials, cleanup=True)
                else:
                    downloader.done.emit(str(file))
            return True
        else:
            return False

    def available(self):
        return True

    def find_tiles(self):
        bounds = transform_extent(targetcrs="EPSG:4326")

        left = int(math.floor(bounds[0]))
        bottom = int(math.floor(bounds[1]))
        right = int(math.ceil(bounds[2]))
        top = int(math.ceil(bounds[3]))

        lat_diff = abs(top - bottom)
        lon_diff = abs(right - bottom)
        tiles = []

        TILE_SIZE = 5

        def _floor(number, mod=TILE_SIZE):
            return number - (number % mod)

        def _ceil(number, mod=TILE_SIZE):
            return number - (number % mod) + mod

        for lat in range(_floor(bottom), _ceil(top), TILE_SIZE):
            for lon in range(_floor(left), _ceil(right), TILE_SIZE):
                lat_tx = _floor(lat)
                lon_tx = _floor(lon)

                if lat_tx < 0:
                    lat_letter = "s"
                else:
                    lat_letter = "n"
                if lon_tx < 0:
                    lon_letter = "w"
                else:
                    lon_letter = "e"

                tilename = f"{lat_letter}{lat_tx:02}{lon_letter}{lon_tx:03}"
                print(tilename, lat, lon)
                if tilename in self.available_tiles():
                    tile = f"eudem_dem_5deg_{tilename}.tif"
                    if tile not in tiles:
                        tiles.append(tile)

        return tiles

    def available_tiles(self):
        available_tiles = (
            "n25w015",
            "n25w020",
            "n30e020",
            "n30e025",
            "n30e030",
            "n30w020",
            "n35e000",
            "n35e005",
            "n35e010",
            "n35e015",
            "n35e020",
            "n35e025",
            "n35e030",
            "n35e035",
            "n35e040",
            "n35w005",
            "n35w010",
            "n40e000",
            "n40e005",
            "n40e010",
            "n40e015",
            "n40e020",
            "n40e025",
            "n40e030",
            "n40e035",
            "n40e040",
            "n40w005",
            "n40w010",
            "n45e000",
            "n45e005",
            "n45e010",
            "n45e015",
            "n45e020",
            "n45e025",
            "n45w005",
            "n45w010",
            "n50e000",
            "n50e005",
            "n50e010",
            "n50e015",
            "n50e020",
            "n50e025",
            "n50w005",
            "n50w010",
            "n50w015",
            "n55e000",
            "n55e005",
            "n55e010",
            "n55e015",
            "n55e020",
            "n55e025",
            "n55w005",
            "n55w010",
            "n60e000",
            "n60e005",
            "n60e010",
            "n60e015",
            "n60e020",
            "n60e025",
            "n60e030",
            "n60w005",
            "n60w015",
            "n60w025",
            "n60w020",
            "n65e010",
            "n65e015",
            "n65e020",
            "n65e025",
            "n65e030",
            "n65w015",
            "n65w020",
            "n65w025",
            "n70e015",
            "n70e020",
            "n70e025",
            "n70e030",
            "n35w030",
            "n35w035",
        )
        return available_tiles
