"""
    Allows loading in .txt files from Terrain Builder format as shapefile
"""

from pathlib import Path
from typing import Generator

from PyQt5.QtCore import QVariant
from qgis.utils import iface
from qgis.core import QgsFields, QgsField
from qgis.core import QgsCoordinateReferenceSystem, QgsVectorFileWriter, QgsWkbTypes
from qgis.core import QgsFeature, QgsGeometry, QgsPointXY

from .tb import TbRow, tb_iterator
from .functions import select_file
from .library import TbLibraryCollection


def import_file(library: TbLibraryCollection):
    path = select_file()
    if path:
        created = create_layer(path, library)
        iface.messageBar().pushSuccess("OBJECTS", f"Import succesful, created {created} objects")


def create_object_writer(path: Path, crs_id: str, fields: QgsFields):
    """Creates a shapefile writer"""
    target_crs = QgsCoordinateReferenceSystem(crs_id)
    writer = QgsVectorFileWriter(str(path), "utf-8", fields, QgsWkbTypes.Point, target_crs, "ESRI Shapefile")
    return writer


def create_layer(path: Path, library: TbLibraryCollection) -> int:
    output = path.parent / f"{path.stem}.shp"
    utm = "EPSG:32631"
    fields = get_fields()
    writer = create_object_writer(output, utm, fields)

    i = 0

    entry: TbRow
    for entry in tb_iterator(path):
        feature = QgsFeature(fields, i)
        feature.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(entry.x, entry.y)))

        x, y, z = library.get_entry(entry.model).size
        category = library.get_category(entry.model)
        feature.setAttributes(
            [i, entry.x, entry.y, entry.z, entry.model, entry.scale, x, y, category,]
        )
        writer.addFeature(feature)
        i += 1

    iface.addVectorLayer(str(output), "gtt_", "ogr")
    del writer

    return i


def get_fields() -> QgsFields:
    """Creates a shapefile layer from a TerrainBuilder .txt file"""
    fields = QgsFields()
    fields.append(QgsField("ID", QVariant.Int))
    fields.append(QgsField("x", QVariant.Double))
    fields.append(QgsField("y", QVariant.Double))
    fields.append(QgsField("z", QVariant.Double))
    fields.append(QgsField("dir", QVariant.String))
    fields.append(QgsField("model", QVariant.String))
    fields.append(QgsField("scale", QVariant.Double))
    fields.append(QgsField("library", QVariant.String))
    fields.append(QgsField("width", QVariant.Double))
    fields.append(QgsField("height", QVariant.Double))
    return fields
