from PyQt5.QtCore import QObject
from qgis.utils import iface
from gtt.extent import get_working_layer, extent_error, transform_extent

from .downloader import OsmDownloader
from .handling import OsmHandling


def download_osm():
    downloader = OsmDownload()
    downloader.add_osm()


class OsmDownload(QObject):
    def __init__(self):
        super().__init__(parent=iface)

        self.handling = OsmHandling(parent=self)
        self.downloader = OsmDownloader(parent=self)
        self.downloader.done.connect(self.handling.run)


    def add_osm(self, *args, **kwargs):
        """Downloads OSM roads and polgyons for marked extent"""
        if get_working_layer() is None:
            return extent_error()

        bounds = transform_extent("EPSG:4326")
        downloader = self.downloader.download(bounds)
