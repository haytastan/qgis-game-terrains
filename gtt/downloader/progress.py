from gtt.functions import show_progress, message_log
from qgis.utils import iface


class ProgressBar:
    def __init__(self, text="heightmap"):
        self.text = text
        self._progress = show_progress(f"Downloading {text}", range_=(0, 100))

    def progress(self, progress: float):
        if self._progress is not None:
            self._progress.bar.setValue(progress)
        if progress is None:  # Hide
            pass

    def done(self, file: str, *args):
        if self._progress is not None:
            self._progress.setText(f"Downloaded {self.text}")
            self._progress.bar.setValue(100)

    def cancel(self, *args):
        if self._progress is not None:
            self._progress.setText("Download cancelled")
            self._progress.bar.setRange(0, 0)
            self._progress.setDuration(5)

    def error(self, title: str, msg: str):
        if self._progress is not None:
            iface.messageBar().popWidget(self._progress)
            self._progress = None
        iface.messageBar().pushCritical(title, msg)
