<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" maxScale="0" hasScaleBasedVisibilityFlag="0" minScale="1e+08" version="3.6.0-Noosa">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer opacity="1" type="singlebandpseudocolor" classificationMax="237" band="1" classificationMin="-8" alphaBand="-1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>MinMax</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <rastershader>
        <colorrampshader clip="0" colorRampType="INTERPOLATED" classificationMode="1">
          <colorramp name="[source]" type="cpt-city">
            <prop v="0" k="inverted"/>
            <prop v="cpt-city" k="rampType"/>
            <prop v="jm/sd/sd-a" k="schemeName"/>
            <prop v="" k="variantName"/>
          </colorramp>
          <item value="-8" color="#334392" alpha="255" label="-8"/>
          <item value="-2.8795" color="#4a73be" alpha="255" label="-2.88"/>
          <item value="2.241" color="#6e8df3" alpha="255" label="2.24"/>
          <item value="7.337" color="#94fe85" alpha="255" label="7.34"/>
          <item value="12.4575" color="#9efe87" alpha="255" label="12.5"/>
          <item value="17.578" color="#adfd88" alpha="255" label="17.6"/>
          <item value="22.6985" color="#b4fe8b" alpha="255" label="22.7"/>
          <item value="27.819" color="#bdfe8c" alpha="255" label="27.8"/>
          <item value="32.9395" color="#c5fd8d" alpha="255" label="32.9"/>
          <item value="38.0355" color="#cffe90" alpha="255" label="38"/>
          <item value="43.156" color="#d7fe92" alpha="255" label="43.2"/>
          <item value="48.2765" color="#dffe93" alpha="255" label="48.3"/>
          <item value="53.397" color="#e7fe95" alpha="255" label="53.4"/>
          <item value="58.5175" color="#eefd97" alpha="255" label="58.5"/>
          <item value="63.6135" color="#f7fe9a" alpha="255" label="63.6"/>
          <item value="68.734" color="#fefe9b" alpha="255" label="68.7"/>
          <item value="73.8545" color="#fdfc97" alpha="255" label="73.9"/>
          <item value="78.975" color="#fef895" alpha="255" label="79"/>
          <item value="84.0955" color="#fdf292" alpha="255" label="84.1"/>
          <item value="89.216" color="#feee92" alpha="255" label="89.2"/>
          <item value="94.312" color="#fde98c" alpha="255" label="94.3"/>
          <item value="99.4325" color="#fee187" alpha="255" label="99.4"/>
          <item value="104.553" color="#fddb7f" alpha="255" label="105"/>
          <item value="109.6735" color="#fed779" alpha="255" label="110"/>
          <item value="114.794" color="#fdce7d" alpha="255" label="115"/>
          <item value="119.89" color="#facb80" alpha="255" label="120"/>
          <item value="125.0105" color="#f8c583" alpha="255" label="125"/>
          <item value="130.131" color="#f6c586" alpha="255" label="130"/>
          <item value="135.2515" color="#f4c588" alpha="255" label="135"/>
          <item value="140.372" color="#f2be8b" alpha="255" label="140"/>
          <item value="145.4925" color="#f0bc8f" alpha="255" label="145"/>
          <item value="150.5885" color="#eebb91" alpha="255" label="151"/>
          <item value="155.709" color="#ebbc99" alpha="255" label="156"/>
          <item value="160.8295" color="#ebc2a4" alpha="255" label="161"/>
          <item value="165.95" color="#e8c5ac" alpha="255" label="166"/>
          <item value="171.0705" color="#e6c8b1" alpha="255" label="171"/>
          <item value="176.1665" color="#dfc6b3" alpha="255" label="176"/>
          <item value="181.287" color="#ddc7b7" alpha="255" label="181"/>
          <item value="186.4075" color="#e0cfc2" alpha="255" label="186"/>
          <item value="191.528" color="#e4d6cb" alpha="255" label="192"/>
          <item value="196.6485" color="#e8ddd4" alpha="255" label="197"/>
          <item value="201.7445" color="#ebe2db" alpha="255" label="202"/>
          <item value="206.865" color="#efe7e1" alpha="255" label="207"/>
          <item value="211.9855" color="#f3eeea" alpha="255" label="212"/>
          <item value="217.106" color="#f6f0ec" alpha="255" label="217"/>
          <item value="222.2265" color="#faf9f8" alpha="255" label="222"/>
          <item value="227.347" color="#ffffff" alpha="255" label="227"/>
          <item value="237" color="#ffffff" alpha="255" label="237"/>
        </colorrampshader>
      </rastershader>
    </rasterrenderer>
    <brightnesscontrast contrast="3" brightness="5"/>
    <huesaturation colorizeBlue="128" colorizeRed="255" grayscaleMode="0" colorizeOn="0" saturation="-6" colorizeGreen="128" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
