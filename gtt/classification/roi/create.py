"""
    Used to create new regions of interest shapes, setting the attributes for them
"""

from functools import partial

from qgis.core import Qgis, QgsVectorLayerTools, QgsWkbTypes, QgsGeometry, QgsPointXY
from qgis.core import QgsFeature, QgsPolygon, QgsFields, QgsField, QgsVectorLayer
from qgis.utils import iface
from qgis.gui import QgsMapToolCapture, QgsMapTool, QgsMapMouseEvent
from qgis.gui import QgsRubberBand, QgisInterface, QgsMapCanvas

from PyQt5.QtCore import QVariant, Qt, pyqtSignal
from PyQt5.QtGui import QColor, QCursor, QPixmap
from typing import List

from gtt.functions import (
    get_layer,
    move_layer_to_top,
    message_log,
)
from .functions import create_layer


def create(surface: str):
    """Activates the tool to create a polygon, once closed will save polygon as given surface type
    
    Args:
        surface (str): Surface name
    """

    layer = create_layer()
    if layer is None:
        return

    create_feature(layer, surface)


def create_feature(layer, surface):
    instructions()
    tool = CreateSampleTool(iface.mapCanvas(), layer)
    tool.set_data(surface, layer)
    tool.done.connect(add_feature)
    tool.start()


def instructions():
    iface.messageBar().pushMessage("Left click to add point, right click to finish shape")


def add_feature(coords: List[tuple], data: dict) -> QgsFeature:
    """Adds feature to saved layer
    
    Args:
        coords (List[tuple]): List of x,y coordinates in layer coordinate space
        data (dict): Saved data to create feature with (layer and class)

        Returns:
            QgsFeature: created feature
    """

    layer = data["layer"]  # type: QgsVectorLayer
    layer.beginEditCommand("Add ROI Sample")

    layer = get_layer("gtt_roi")  # type: QgsVectorLayer

    feat = QgsFeature(layer.fields())
    feat.setAttribute("class", data["class"])
    points = [QgsPointXY(x, y) for x, y in coords]
    geom = QgsGeometry.fromPolygonXY([points])
    feat.setGeometry(geom)
    layer.dataProvider().addFeatures([feat])

    iface._temp1 = points
    iface._temp2 = geom
    iface._temp3 = feat

    move_layer_to_top(layer)
    layer.endEditCommand()
    layer.triggerRepaint()
    iface.mapCanvas().refresh()
    return feat


def create_cursor():
    return QCursor(
        QPixmap(
            [
                "16 16 3 1",
                "      c None",
                ".     c #FF0000",
                "+     c #800080",
                "                ",
                "       +.+      ",
                "      ++.++     ",
                "     +.....+    ",
                "    +.  .  .+   ",
                "   +.   .   .+  ",
                "  +.    .    .+ ",
                " ++.    .    .++",
                " ... ...+... ...",
                " ++.    .    .++",
                "  +.    .    .+ ",
                "   +.   .   .+  ",
                "   ++.  .  .+   ",
                "    ++.....+    ",
                "      ++.++     ",
                "       +.+      ",
            ]
        )
    )


class CreateSampleTool(QgsMapTool):
    done = pyqtSignal(list, dict)
    stop = pyqtSignal(list)

    # rubberband: QgsRubberBand
    geometry: QgsGeometry
    points: List[tuple]
    canvas: QgsMapCanvas
    rubberband: QgsRubberBand

    def __init__(self, canvas, layer):
        super().__init__(canvas)
        self.canvas = canvas
        self.layer = layer
        self.points = []
        self.capturing = False
        self.rubberband = None
        self.geometry = None

    def set_data(self, clas, layer):
        self.data = {"class": clas, "layer": layer}

    def isCapturing(self):
        return self.capturing

    def start(self):
        self.canvas.setMapTool(self)
        self.canvas.setCursor(create_cursor())

    def startCapturing(self):
        self.points = []
        self.capturing = True
        self._create_rubberband()

    def stopCapturing(self):
        self.capturing = False
        self.canvas.unsetMapTool(self)

        if self.rubberband is None:
            return

        self.rubberband.closePoints()
        self.geometry = self.rubberband.asGeometry()
        self.rubberband.reset()
        self.rubberband = None
        self.done.emit(self.points, self.data)

    def addVertex(self, coords: tuple):
        """Adds a point to the rubberband
        
        Args:
            coords (tuple): x,y coordinates in canvas coordinate space
        """
        point = QgsPointXY(*coords)
        self.rubberband.addPoint(point)

    def canvasReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            if not self.isCapturing():
                self.startCapturing()

            point, layer_point = self.transformCoordinates(event.pos())
            # point = event.snapPoint(QgsMapMouseEven/t.SnapProjectConfig)
            self.addVertex(point)
            self.points.append(layer_point)

        elif event.button() == Qt.RightButton:
            self.stopCapturing()

    def transformCoordinates(self, canvas_point):
        return (
            self.toMapCoordinates(canvas_point),
            self.toLayerCoordinates(self.layer, canvas_point),
        )

    def _create_rubberband(self):
        self.rubberband = QgsRubberBand(self.canvas, QgsWkbTypes.PolygonGeometry)
        self.rubberband.setColor(QColor(255, 0, 0, 255))
        self.rubberband.setFillColor(QColor(0, 0, 0, 100))
        self.rubberband.setWidth(1)
