from qgis.utils import iface
from pathlib import Path

from . import functions
from gtt.downloader import unzip, Download
from gtt.classification.install import url


def test_fulldisk_download():
    target_file = functions.full_disk() / "orfeo.zip"  # type: Path
    download = Download(parent=iface)
    download.get_file(url, target_file, target_file.parent, cleanup=True)
    download.error.connect(iface.messageBar().pushCritical)


def test_fulldisk_unzip():
    zipfile = Path(__file__).parent / "data" / "empty.zip"
    unzip(str(zipfile), functions.full_disk(), cleanup=False)
