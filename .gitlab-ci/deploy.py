import datetime
import os
import sys
from pathlib import Path

from api.release import run
from api.wiki import update_home, update_page
from functions import get_last_tag

def update_wiki_release(files, name: str):
    print("## Updating wiki home page")
    print("Files:")
    print(files)
    with (Path(__file__).parents[1] / "README.md").open() as fp:
        data = fp.read()

    now = datetime.datetime.now()
    time = str(now.strftime("%m-%d-%y %H:%M:%S"))
    data = data.replace("{{download}}", files)
    data = data.replace("{{time}}", time)
    update_home(data)


def update_wiki_version(name: str):
    version = get_last_tag()
    update_page("version", version)


if __name__ == "__main__":

    yaml_file = Path(__file__).parent / ".gitlab-ci-release.yml"
    success, result = run(yaml_file)
    name = sys.argv[1]
    if success:
        files = result["files"]
        changelog = result["changelog"]
        update_wiki_release(files, name)
        update_wiki_version(name)
        update_page("changelog", changelog)
