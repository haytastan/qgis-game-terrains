import sys
from pathlib import Path

from functions import load_environment
from build import bump_version, create_zip

if __name__ == "__main__":
    load_environment()

    source = Path(__file__).parent
    bump_version(source)
    create_zip(source, filename=source.parent.stem)
